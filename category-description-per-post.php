<?php
/*
Plugin Name: Category description per post
Plugin URI: https://bitbucket.org/austinjp/category-description-per-post
Description: Inject the category name and description above the title of the post. Only displayed if the category description is not blank. Not displayed on certain pages (archives, search results, etc).
Version: 0.1.0
Author: Austin Plunkett
Author URI: https://bitbucket.org/austinjp
*/

function inject_category_name_and_description($query) {
    $post_id = get_the_ID();
    $cats = get_the_category($post_id);
    if (empty($cats) || !isset($post_id)) { return; }

    // FIXME Make this configurable in options panel.
    $no_display = array(
	'footer.php'
    );

    foreach ($no_display as $f) {
	if (verify_caller_file($f)) {
	    return;
	}
    }

    if (should_display_category_info($post_id, $cats)) {
	echo '<div class="category-info">' . "\n"
	    . '<h2>' . $cats[0]->name . '</h2>' . "\n"
	    . '<p>' . $cats[0]->category_description . '</p>' . "\n"
	    . '</div>' . "\n";
    }
}
add_action('loop_start', 'inject_category_name_and_description');

function should_display_category_info($post_id = null, $cats = null) {
    if ($post_id == null || $cats == null) { return false; }
    if ($cats[0]->category_description == '' || $cats[0]->category_description == null || !isset($cats[0]->category_description)) { return false; }

    // FIXME Form saving is failing
    // if(get_post_meta($post_id,'category_info_class', true) &&
    if (is_home() ||
	is_front_page() ||
	is_404() ||
	is_archive() ||
	is_attachment() ||
	is_author() ||
	is_month() ||
	is_search() ||
	is_tag() ||
	is_year()) { return false; }

/*    
    if (get_post_type($post_id) != 'post' ||
	get_post_type($post_id) != 'page') {
	return false;
    }
    
    if (is_singular(array('post','page'))) {
	return false;
    }
*/

    return true;
}

add_action('load-post.php','category_info_metabox_setup');
add_action('load-post-new.php','category_info_metabox_setup');

function category_info_metabox_setup() {
    add_action('add_meta_boxes','category_info_metabox');
    add_action('save_post','category_info_save');
}

function category_info_metabox() {
    add_meta_box(
        'category_info_class',         // ID
        'Category info',               // title to display
        'category_info_metabox_html',  // callback func
        [ 'post', 'page' ],            // post types
        'side',                        // context
        'default'                      // priority
    );
}

function category_info_metabox_html() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'category_info_class_nonce' ); ?>
    <p>
      <label><input name="category_info_class" id="category_info_class" type="checkbox" value="1" <?php if(get_post_meta($post->ID,'category_info_class', true) != 0) { echo 'checked'; } ?>>Display category information on this post</label>
    </p>
    <p>
      Update or save the post to set.
    </p>
<?php }

function category_info_save($post_id, $post = null) {
    if (null == $post) { return; }
    if ( !isset( $_POST['category_info_class_nonce'] ) || !wp_verify_nonce( $_POST['category_info_class_nonce'], basename( __FILE__ ) ) )
        return $post_id;

    $post_type = get_post_type_object( $post->post_type );

    /* Check if the current user has permission to edit the post. */
    if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
        return $post_id;

    $new_meta_value = isset( $_POST['category_info_class'] ) ? true : false;

    $meta_key = 'category_info_class';

    if ($new_meta_value) {
        add_post_meta( $post_id, $meta_key, 1, true );
    } else {
        delete_post_meta($post_id, $meta_key);
    }
}

/* Courtesy of https://wordpress.stackexchange.com/a/228233 */
function verify_caller_file($file_name, $files = array(), $dir = '') {
    if( empty( $files ) ) {
        $files = debug_backtrace();
    }

    if( ! $dir ) {
        $dir = get_stylesheet_directory() . '/';
    }

    $caller_theme_file = array();

    foreach( $files as $file ) {
	if (array_key_exists('file',$file)) {
            if( false !== mb_strpos($file['file'], $dir) ) {
		$caller_theme_file[] = $file['file'];
	    }
	}
    }

    if( $file_name ) {
        return in_array( $dir . $file_name, $caller_theme_file );
    }

    return;
}
